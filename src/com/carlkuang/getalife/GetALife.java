/**
 * 
 */
package com.carlkuang.getalife;

import org.newdawn.slick.Animation;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.ScalableGame;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import com.carlkuang.getalife.helper.UtilLogger;
import com.carlkuang.getalife.helper.UtilLogger.LogLevel;

/**
 * @author WiseClock
 * @version 0.1
 */
public class GetALife extends BasicGame
{
    Image imgGroundTop;
    Image imgGroundBottom;
    float groundPosX = 0;
    Image imgLogo;
    SpriteSheet playerSheet;
    Animation player;
    int count = 0;

    public GetALife(String title)
    {
        super(title);
    }

    public void init(GameContainer container) throws SlickException
    {
        ModLoader.load();
        FontFactory.initialize();
        imgGroundTop = new Image("com/carlkuang/getalife/asset/image/groundTop.png");
        imgGroundBottom = new Image("com/carlkuang/getalife/asset/image/groundBottom.png");
        imgLogo = new Image("com/carlkuang/getalife/asset/image/menuLogo.png");
        playerSheet = new SpriteSheet("com/carlkuang/getalife/asset/image/player_o.png", 110, 150);
        player = new Animation(playerSheet, 0, 0, 6, 0, true, 100, true);
        for (int i = 0; i < 5; i++)
        {
            player.addFrame(playerSheet.getSprite(i, 1), 100);
        }
        UtilLogger.log(LogLevel.INFO, "Game initialized.");
    }

    public void update(GameContainer container, int delta) throws SlickException
    {
        groundPosX -= delta * 0.1;
        groundPosX = groundPosX % 64;
        if(container.getInput().isKeyDown(Input.KEY_ENTER)){
            AppGameContainer gc = (AppGameContainer) container;
            switch (count)
            {
                case 0:
                    gc.setDisplayMode(1920, 1080, false);
                    count++;
                    break;
                case 1:
                    gc.setDisplayMode(1600, 900, false);
                    count++;
                    break;
                case 2:
                    gc.setDisplayMode(1280, 720, false);
                    count++;
                    break;
                case 3:
                    gc.setDisplayMode(800, 600, false);
                    count++;
                    break;
                case 4:
                    gc.setDisplayMode(640, 480, false);
                    count = 0;
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void mousePressed(int button, int x, int y)
    {
        System.out.println("click " + x + " " + y);
    }

    public void render(GameContainer container, Graphics graphics) throws SlickException
    {
        //float currentHeight = container.getHeight();
        //float scale = (float)(Math.round(currentHeight / 1080 * 100)) / 100;
        //graphics.scale(scale, scale);

        graphics.setBackground(Color.decode("8675152"));
        graphics.clear();

        //imgLogo.setFilter(Image.FILTER_NEAREST);
        imgLogo.drawCentered(960, 200);

        FontFactory.YAHEI.drawString(850, 242, "当个现充", Color.white);

        player.draw((1920 - 110) / 2, 1080 - 150 - 128 + 13, 110, 150); // FML so many magic numbers... for testing purpose

        graphics.drawImage(imgGroundTop, groundPosX, 952, 1920, 1016, 0, 0, 1920 - groundPosX, 64);
        graphics.drawImage(imgGroundBottom, groundPosX, 1016, 1920, 1080, 0, 0, 1920 - groundPosX, 64);
    }

    public static void main(String[] argv) throws SlickException
    {
        //AppGameContainer game = new AppGameContainer(new GetALife("当个现充"));
        AppGameContainer game = new AppGameContainer(new ScalableGame(new GetALife("当个现充"), 1920, 1080, true));
        game.setDisplayMode(1280, 720, false);
        game.setMaximumLogicUpdateInterval(60);
        //game.setTargetFrameRate(60);
        game.setAlwaysRender(true);
        //game.setVSync(true);
        game.setShowFPS(true);
        game.start();
    }
}
