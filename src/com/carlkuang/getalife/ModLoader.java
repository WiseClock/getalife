/**
 * 
 */
package com.carlkuang.getalife;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import com.carlkuang.getalife.annotation.Init;
import com.carlkuang.getalife.annotation.Mod;
import com.carlkuang.getalife.annotation.PreInit;
import com.carlkuang.getalife.exception.ModAlreadyExistsException;
import com.carlkuang.getalife.helper.UtilLogger;
import com.carlkuang.getalife.helper.UtilLogger.LogLevel;
import com.carlkuang.getalife.mod.BasicMod;
import com.carlkuang.getalife.mod.BuiltInMod;

/**
 * @author WiseClock
 * @version 0.1
 */
public final class ModLoader
{
    private static HashMap<String, ModStruct> modList = new HashMap<String, ModStruct>();

    private ModLoader()
    {
    }

    protected static class ModStruct
    {
        private String id;
        private String name;
        private String version;
        private BasicMod mod;
        public ModStruct(String id, String name, String version, BasicMod mod)
        {
            this.id = id;
            this.name = name;
            this.version = version;
            this.mod = mod;
        }
        public String getId()
        {
            return id;
        }
        public String getName()
        {
            return name;
        }
        public String getVersion()
        {
            return version;
        }
        public BasicMod getMod()
        {
            return mod;
        }
    }

    protected static HashMap<String, ModStruct> getModList()
    {
        return modList;
    }

    protected static void load()
    {
        ArrayList<BasicMod> modToBeLoaded = new ArrayList<BasicMod>();
        modToBeLoaded.add(new BuiltInMod());

        for (BasicMod mod : modToBeLoaded)
        {
            Constructor<?>[] constructors = mod.getClass().getConstructors();
            for (Constructor<?> constructor : constructors)
            {
                if (constructor.isAnnotationPresent(Mod.class))
                {
                    Mod modAnnotation = (Mod) constructor.getAnnotation(Mod.class);
                    String modUniqueId = modAnnotation.uniqueId();
                    String modName = modAnnotation.name();
                    String modVersion = modAnnotation.version();
                    ModStruct modStruct = new ModStruct(modUniqueId, modName, modVersion, mod);
                    try
                    {
                        addMod(modStruct);
                    }
                    catch (ModAlreadyExistsException e)
                    {
                        UtilLogger.log(LogLevel.WARNING, e.getMessage());
                    }
                    break;
                }
            }
        }

        UtilLogger.log(LogLevel.INFO, modList.size() + (modList.size() > 1 ? " mods" : " mod") + " loaded.");

        for (ModStruct modStruct : modList.values())
        {
            executeMod(modStruct);
        }
    }

    private static void addMod(ModStruct modStruct) throws ModAlreadyExistsException
    {
        if (modList.containsKey(modStruct.getId()))
        {
            throw new ModAlreadyExistsException("This mod already exists.");
        }
        else
        {
            modList.put(modStruct.getId(), modStruct);
            Method[] methods = modStruct.getMod().getClass().getDeclaredMethods();
            for (Method method : methods)
            {
                if (method.isAnnotationPresent(PreInit.class))
                {
                    try
                    {
                        method.invoke(modStruct.getMod(), new Object[] {});
                    }
                    catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
                    {
                        UtilLogger.log(LogLevel.WARNING, e.getMessage());
                    }
                    break;
                }
            }
        }
    }

    private static void executeMod(ModStruct modStruct)
    {
        Method[] methods = modStruct.getMod().getClass().getDeclaredMethods();
        for (Method method : methods)
        {
            if (method.isAnnotationPresent(Init.class))
            {
                try
                {
                    method.invoke(modStruct.getMod(), new Object[] {});
                }
                catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
                {
                    UtilLogger.log(LogLevel.WARNING, e.getMessage());
                }
                break;
            }
        }
    }
}
