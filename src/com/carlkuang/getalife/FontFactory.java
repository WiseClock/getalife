/**
 * 
 */
package com.carlkuang.getalife;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.GlyphPage;
import org.newdawn.slick.font.effects.ColorEffect;

import com.carlkuang.getalife.helper.UtilLogger;
import com.carlkuang.getalife.helper.UtilLogger.LogLevel;

/**
 * @author WiseClock
 * @version 0.1
 */
public class FontFactory
{
    public static final UnicodeFont YAHEI = createFont(new Font("Microsoft Yahei", Font.BOLD, 22), true, true);
    //public static final UnicodeFont  TAHOMA = createFont(new Font("Tahoma", Font.PLAIN, 16), false);

    private FontFactory()
    {
    }

    public static final void initialize()
    {
    }

    @SuppressWarnings("unchecked")
    private static UnicodeFont createFont(Font awtFont, boolean unicode, boolean antiAlias)
    {
        if (!antiAlias)
        {
            java.awt.Graphics g = GlyphPage.getScratchGraphics();
            if (g!=null && g instanceof Graphics2D)
            {
                Graphics2D g2d = (Graphics2D)g;
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            }
        }

        UnicodeFont font;
        font = new UnicodeFont(awtFont);
        font.getEffects().add(new ColorEffect(Color.WHITE));
        font.addAsciiGlyphs();
        if (unicode)
        {
            font.addGlyphs(19968, 40959);
        }
        try
        {
            font.loadGlyphs();
        }
        catch (SlickException e)
        {
            UtilLogger.log(LogLevel.ERROR, e.getMessage());
        }
        return font;
    }
}
