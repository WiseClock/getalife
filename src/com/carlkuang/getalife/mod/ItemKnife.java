/**
 * 
 */
package com.carlkuang.getalife.mod;

import com.carlkuang.getalife.api.IItem;

/**
 * @author WiseClock
 * @version 0.1
 */
public class ItemKnife implements IItem
{

    @Override
    public String getItemId()
    {
        return "knife";
    }

    @Override
    public String getItemDefaultName()
    {
        return "Knife小刀";
    }

}
