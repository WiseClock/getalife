/**
 * 
 */
package com.carlkuang.getalife.mod;

import com.carlkuang.getalife.annotation.Init;
import com.carlkuang.getalife.annotation.Mod;

/**
 * @author WiseClock
 * @version 0.1
 */
public class BuiltInMod extends BasicMod
{
    @Mod(name = "Built in mod", uniqueId = "com.carlkuang.moeuniverseoffline.builtinmod", version = "0.1")
    public BuiltInMod()
    {
    }

    @Init
    public void init()
    {
        this.registerItem(new ItemKnife());
    }
}
