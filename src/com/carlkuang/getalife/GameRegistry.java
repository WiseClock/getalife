/**
 * 
 */
package com.carlkuang.getalife;

import com.carlkuang.getalife.api.IItem;
import com.carlkuang.getalife.exception.ItemAlreadyExistsException;
import com.carlkuang.getalife.helper.UtilLogger;
import com.carlkuang.getalife.helper.UtilLogger.LogLevel;
import com.carlkuang.getalife.mod.BasicMod;

/**
 * @author WiseClock
 * @version 0.1
 */
public final class GameRegistry
{
    private GameRegistry()
    {
    }

    public static void registerItem(BasicMod mod, IItem item)
    {
        try
        {
            Item.addItem(mod, item);
        }
        catch (ItemAlreadyExistsException e)
        {
            UtilLogger.log(LogLevel.WARNING, e.getMessage());
        }
    }
}
