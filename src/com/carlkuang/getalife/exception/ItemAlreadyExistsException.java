/**
 * 
 */
package com.carlkuang.getalife.exception;

/**
 * @author WiseClock
 * @version 0.1
 */
public class ItemAlreadyExistsException extends Exception
{
    private static final long serialVersionUID = 1L;

    public ItemAlreadyExistsException(String msg)
    {
        super(msg);
    }

    public ItemAlreadyExistsException(String msg, Throwable throwable)
    {
        super(msg, throwable);
    }
}
