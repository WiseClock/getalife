/**
 * 
 */
package com.carlkuang.getalife.exception;

/**
 * @author WiseClock
 * @version 0.1
 */
public class ModAlreadyExistsException extends Exception
{
    private static final long serialVersionUID = 1L;

    public ModAlreadyExistsException(String msg)
    {
        super(msg);
    }

    public ModAlreadyExistsException(String msg, Throwable throwable)
    {
        super(msg, throwable);
    }
}
