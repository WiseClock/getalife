/**
 * 
 */
package com.carlkuang.getalife;

import java.lang.reflect.Constructor;
import java.util.HashMap;

import com.carlkuang.getalife.annotation.Mod;
import com.carlkuang.getalife.api.IItem;
import com.carlkuang.getalife.exception.ItemAlreadyExistsException;
import com.carlkuang.getalife.mod.BasicMod;

/**
 * @author WiseClock
 * @version 0.1
 */
public final class Item
{
    private static HashMap<String, IItem> itemList = new HashMap<String, IItem>();

    private Item()
    {
    }

    protected static HashMap<String, IItem> getItemList()
    {
        return itemList;
    }

    protected static void addItem(BasicMod mod, IItem item) throws ItemAlreadyExistsException
    {
        String modUniqueId = null;
        Constructor<?>[] constructors = mod.getClass().getConstructors();
        for (Constructor<?> constructor : constructors)
        {
            if (constructor.isAnnotationPresent(Mod.class))
            {
                Mod modAnnotation = (Mod) constructor.getAnnotation(Mod.class);
                modUniqueId = modAnnotation.uniqueId();
                break;
            }
        }
        if (itemList.containsKey(modUniqueId + "." + item.getItemId()))
        {
            throw new ItemAlreadyExistsException("This item already exists.");
        }
        else
        {
            itemList.put(modUniqueId + "." + item.getItemId(), item);
        }
    }
}
