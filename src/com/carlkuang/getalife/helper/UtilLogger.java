/**
 * 
 */
package com.carlkuang.getalife.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author WiseClock
 * @version 1.0
 */
public class UtilLogger
{
    private static boolean enableDev = false;
    
    public enum LogLevel
    {
        DEV("DEV"),
        INFO("INFO"),
        WARNING("WARNING"),
        ERROR("ERROR");

        private final String level;

        LogLevel(String lvl)
        {
            level = lvl;
        }

        @Override
        public String toString()
        {
            return level;
        }
    }

    /**
     * Logs the string to the specified level of category.
     * @param category the desired level of {@link EnumCategory category}.
     * @param str the string to be logged.
     */
    public static void log(LogLevel level, String str)
    {
        if(level != LogLevel.DEV || enableDev)
        {
            System.out.println(getTime() + " [" + level + "]\t" + str);
        }
    }

    /**
     * Gets the current time.
     * @return the current time.
     */
    private static String getTime()
    {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        return "[" + formatter.format(date.getTime()) + "]";
    }

    /**
     * Sets if dev output is enabled.
     * @param flag enable dev output.
     */
    public static void setEnableDev(boolean flag)
    {
        enableDev = flag;
    }
}
